import numpy as np

def Latency():
    inp = np.loadtxt('input_unix_timestamps.txt')
    outp = np.loadtxt('output_unix_timestamps.txt')
        
    inp = np.transpose(inp)
    outp = np.transpose(outp)
    
    i_real = inp[1]
    o_real = outp[1]
    i_gps = inp[0]
    o_gps = outp[0]

    e_file = open('tests/tests_pytest/error_results.txt', 'a')
    
    i_lst = [list(i_gps).index(x) for x in list(np.intersect1d(i_gps,o_gps))]   #sub a and b for left and right collumn of output, returns a list of the indexs of a to keep 
    lat = o_real - i_real[i_lst]
    e_file.write("Median latency = %f" % np.median(lat))
    e_file.close()

    assert np.median(lat) < 6
    #assert np.percentile(lat, 99) < 10
    #assert np.max(lat) < 10
