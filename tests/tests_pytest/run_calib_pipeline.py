#!/usr/bin/env python3
# Copyright (C) 2018  Aaron Viets, Alexander Bartoletti
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import os
from threading import Thread

"""
from tests.tests_pytest.State_Vector import State_Vector
from tests.tests_pytest.ASD import ASD
from tests.tests_pytest.act2darm_timeseries import Act2darm
from tests.tests_pytest.pcal2darm_timeseries import Pcal2darm
from tests.tests_pytest.diff import Diff
from tests.tests_pytest.latency import Latency
"""


def calib_C00_thread_function():
    os.system("gstlal_compute_strain --gps-start-time 1370673024 --gps-end-time 1370678464 --frame-cache tests/tests_pytest/raw_frames.cache --output-path tests/tests_pytest/frames/GDS --frame-duration=64 --frames-per-file=1 --wings=0 --config-file tests/tests_pytest/filters/gstlal_compute_strain_C00_offlineTest_H1.ini --filters-file-name tests/tests_pytest/filters/gstlal_compute_strain_C00_filters_H1_20230613_ci.npz")
    os.system("ls tests/tests_pytest/frames/GDS/H-H1GDS-*.gwf | lal_path2cache > tests/tests_pytest/GDS_frames.cache")

def calib_Approx_thread_function():
    os.system("gstlal_compute_strain --gps-start-time 1370673024 --gps-end-time 1370678464 --frame-cache tests/tests_pytest/raw_frames.cache --output-path tests/tests_pytest/frames/GDS --frame-duration=64 --frames-per-file=1 --wings=0 --config-file tests/tests_pytest/filters/gstlal_compute_strain_Approx_H1.ini --filters-file-name tests/tests_pytest/filters/gstlal_compute_strain_C00_filters_H1_20230613_ci.npz")
    os.system("ls tests/tests_pytest/frames/GDS/H-H1GDS_Approx-*.gwf | lal_path2cache > tests/tests_pytest/GDS_Approx_frames.cache")

def calib_Exact_thread_function():
    os.system("gstlal_compute_strain --gps-start-time 1370673024 --gps-end-time 1370678464 --frame-cache tests/tests_pytest/raw_frames.cache --output-path tests/tests_pytest/frames/GDS --frame-duration=64 --frames-per-file=1 --wings=0 --config-file tests/tests_pytest/filters/gstlal_compute_strain_Exact_H1.ini --filters-file-name tests/tests_pytest/filters/gstlal_compute_strain_C00_filters_H1_20230613_ci.npz")
    os.system("ls tests/tests_pytest/frames/GDS/H-H1GDS_Exact-*.gwf | lal_path2cache > tests/tests_pytest/GDS_Exact_frames.cache")

def calib_LaterStart_thread_function():
    os.system("gstlal_compute_strain --gps-start-time 1370673091 --gps-end-time 1370678464 --frame-cache tests/tests_pytest/raw_frames.cache --output-path tests/tests_pytest/frames/GDS --frame-duration=64 --frames-per-file=1 --wings=0 --config-file tests/tests_pytest/filters/gstlal_compute_strain_LaterStart_H1.ini --filters-file-name tests/tests_pytest/filters/gstlal_compute_strain_C00_filters_H1_20230613_ci.npz")
    os.system("ls tests/tests_pytest/frames/GDS/H-H1GDS_LaterStart-*.gwf | lal_path2cache > tests/tests_pytest/GDS_LaterStart_frames.cache")

def calib_Latency_thread_function():
    os.system("gstlal_compute_strain --gps-start-time 1373199776 --gps-end-time 1373200767 --frame-cache tests/tests_pytest/raw_frames.cache --output-path tests/tests_pytest/frames/GDS --frame-duration=1 --frames-per-file=1 --wings=0 --config-file tests/tests_pytest/filters/gstlal_compute_strain_C00_LatencyTest_H1.ini --filters-file-name tests/tests_pytest/filters/gstlal_compute_strain_C00_filters_H1_20230613_ci.npz")

def Run_calib():
    e_file = open('tests/tests_pytest/error_results.txt', 'a')
    e_file.write('running calib pipeline \n')

    os.system("ls tests/tests_pytest/frames/raw/H-H1_R-*.gwf | lal_path2cache > tests/tests_pytest/raw_frames.cache")
    os.system("mkdir -p tests/tests_pytest/frames/GDS")

    os.system("gstlal_compute_strain --gps-start-time 1373199776 --gps-end-time 1373199876 --frame-cache tests/tests_pytest/raw_frames.cache --output-path tests/tests_pytest/frames/GDS --frame-duration=1 --frames-per-file=1 --wings=0 --config-file tests/tests_pytest/filters/gstlal_compute_strain_C00_LatencyTest_H1.ini --filters-file-name tests/tests_pytest/filters/gstlal_compute_strain_C00_filters_H1_20230613_ci.npz")
    """
    calib_C00_thread = Thread(target = calib_C00_thread_function)
    calib_Approx_thread = Thread(target = calib_Approx_thread_function)
    calib_Exact_thread = Thread(target = calib_Exact_thread_function)
    calib_LaterStart_thread = Thread(target = calib_LaterStart_thread_function)
    calib_Latency_thread = Thread(target = calib_Latency_thread_function)

    calib_C00_thread.start()
    calib_Approx_thread.start()
    calib_Exact_thread.start()
    calib_LaterStart_thread.start()
    calib_Latency_thread.start()

    calib_C00_thread.join()
    calib_Approx_thread.join()
    calib_Exact_thread.join()
    calib_LaterStart_thread.join()
    calib_Latency_thread.join()
    """
    f_num = len(os.listdir('tests/tests_pytest/frames/GDS'))
    assert f_num >= 1

    e_file.write('ran calib pipeline \n')
    e_file.close()

    """
    State_Vector_thread = Thread(target = State_Vector)
    ASD_thread = Thread(target = ASD)
    Act2darm_thread = Thread(target = Act2darm)
    Pcal2darm_thread = Thread(target = Pcal2darm)
    Diff_thread = Thread(target = Diff)
    Latency_thread = Thread(target = Latency)

    State_Vector_thread.start()
    ASD_thread.start()
    Act2darm_thread.start()
    Pcal2darm_thread.start()
    Diff_thread.start()
    Latency_thread.start()

    State_Vector_thread.join()
    ASD_thread.join()
    Act2darm_thread.join()
    Pcal2darm_thread.join()
    Diff_thread.join()
    Latency_thread.join()
    """

