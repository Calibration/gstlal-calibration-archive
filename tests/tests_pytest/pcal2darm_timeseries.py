#!/usr/bin/env python3
# Copyright (C) 2018  Aaron Viets, Alexander Bartoletti
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#
# =============================================================================
#
#				   Preamble
#
# =============================================================================
#


import numpy
from math import pi
from math import erf
import resource
import datetime
import glob

import configparser

import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst
Gst.init(None)

import lal
from lal import LIGOTimeGPS

from gstlal import pipeparts
from gstlalcalibration import calibration_parts
from gstlal import simplehandler
from gstlal import datasource

from ligo import segments
from gstlalcalibration import test_common

from tests.tests_pytest.error import rms
from tests.tests_pytest.plot import plot_pcal

from optparse import OptionParser, Option
import configparser

parser = OptionParser()

parser.add_option("--gps-start-time", metavar = "seconds", type = int, default = 1370674240, help = "GPS time at which to start processing data")
parser.add_option("--gps-end-time", metavar = "seconds", type = int, default = 1370678400, help = "GPS time at which to stop processing data")
parser.add_option("--ifo", metavar = "name", default = "H1", help = "Name of the interferometer (IFO), e.g., H1, L1")

options, filenames = parser.parse_args()

ifo = options.ifo


#ifo = 'H1'

#
# Load in the filters file that contains filter coefficients, etc.
#

#filters = numpy.load("tests/tests_pytest/filters/gstlal_compute_strain_C00_filters_H1.npz")
filters = numpy.load("tests/tests_pytest/filters/gstlal_compute_strain_C00_filters_H1_20230613_ci.npz")

# Set up gstlal frame cache list
#gstlal_frame_cache_list = ['tests/tests_pytest/filters/GDS_Approx_frames.cache', 'tests/tests_pytest/filters/GDS_Exact_frames.cache']
gstlal_frame_cache_list = ['tests/tests_pytest/GDS_Approx_frames.cache', 'tests/tests_pytest/GDS_Exact_frames.cache']
gstlal_channels = ['GDS-CALIB_STRAIN', 'GDS-CALIB_STRAIN']

# Set list of all channels
channel_list = []
pcal_channel_name = "CAL-PCALY_RX_PD_OUT_DQ"
channel_list.append((ifo, pcal_channel_name))
pcalx_channel_name = "CAL-PCALX_RX_PD_OUT_DQ"
channel_list.append((ifo, pcalx_channel_name))
for channel in gstlal_channels:
	channel_list.append((ifo, channel))

# Read stuff we need from the filters file
frequencies = []
pcal_corrections = []
pcalx_frequencies = []
pcalx_corrections = []
pcal_line_names = 'ka_pcal,kc_pcal,high_pcal'
for name in pcal_line_names.split(','):
	frequencies.append(float(filters["%s_line_freq" % name]))
	pcal_corrections.append(float(filters["%s_corr_re" % name]))
	pcal_corrections.append(float(filters["%s_corr_im" % name]))

y_arm_pcal_corr = filters['y_arm_pcal_corr']
pcalx_freqs = [410.2,1083.3,33.43,56.39,77.73,102.13,283.91]
x_arm_pcal_corr = -1 * filters['y_arm_pcal_corr']
for f in pcalx_freqs:
	pcalx_frequencies.append(float(f))
	idx1 = int(float(f) * 4)
	idx2 = idx1 + 1
	wt2 = float(f) * 4 - idx1
	wt1 = 1 - wt2
	f1 = y_arm_pcal_corr[0][idx1]
	f2 = y_arm_pcal_corr[0][idx2]
	pcalx_corrections.append((wt1 * x_arm_pcal_corr[1][idx1] * f1**2 + wt2 * x_arm_pcal_corr[1][idx2] * f2**2) / (float(f)**2))
	pcalx_corrections.append((wt1 * x_arm_pcal_corr[2][idx1] * f1**2 + wt2 * x_arm_pcal_corr[2][idx2] * f2**2) / (float(f)**2))

all_frequencies = frequencies + pcalx_frequencies

pcal_time_advance = 0.00006103515625
for i in range(len(pcal_corrections) // 2):
	corr = pcal_corrections[2 * i] + 1j * pcal_corrections[2 * i + 1]
	corr *= numpy.exp(2.0 * numpy.pi * 1j * frequencies[i] * pcal_time_advance)
	pcal_corrections[2 * i] = numpy.real(corr)
	pcal_corrections[2 * i + 1] = numpy.imag(corr)		
for i in range(len(pcalx_corrections) // 2):
	corr = pcalx_corrections[2 * i] + 1j * pcalx_corrections[2 * i + 1]
	corr *= numpy.exp(2.0 * numpy.pi * 1j * pcalx_frequencies[i] * pcal_time_advance)
	pcalx_corrections[2 * i] = numpy.real(corr)
	pcalx_corrections[2 * i + 1] = numpy.imag(corr)

demodulated_pcal_list = []
demodulated_pcalx_list = []
try:
	arm_length = float(filters['arm_length'])
except:
	arm_length = 3995.1

# demodulation and averaging parameters
filter_time = 20
average_time = 128
rate_out = 1

#
# =============================================================================
#
#				  Pipelines
#
# =============================================================================
#


def pcal2darm(pipeline, name):

	# Get pcal from the raw frames
	raw_data = pipeparts.mklalcachesrc(pipeline, location = "tests/tests_pytest/raw_frames.cache", cache_dsc_regex = ifo)
	raw_data = pipeparts.mkframecppchanneldemux(pipeline, raw_data, do_file_checksum = False, skip_bad_files = True, channel_list = list(map("%s:%s".__mod__, channel_list)))
	pcal = calibration_parts.hook_up(pipeline, raw_data, pcal_channel_name, ifo, 1.0)
	pcal = calibration_parts.caps_and_progress(pipeline, pcal, "audio/x-raw,format=F64LE,channels=1,channel-mask=(bitmask)0x0", "pcal")
	pcal = pipeparts.mktee(pipeline, pcal)

	# Are we also looking at PcalX?
	if any(pcalx_frequencies):
		pcalx = calibration_parts.hook_up(pipeline, raw_data, pcalx_channel_name, ifo, 1.0)
		pcalx = calibration_parts.caps_and_progress(pipeline, pcalx, "audio/x-raw,format=F64LE,channels=1,channel-mask=(bitmask)0x0", "pcalx")
		pcalx = pipeparts.mktee(pipeline, pcalx)

	# Demodulate the pcal channel at the lines of interest
	for i in range(len(frequencies)):
		demodulated_pcal = calibration_parts.demodulate(pipeline, pcal, frequencies[i], True, rate_out, filter_time, 0.5, prefactor_real = pcal_corrections[2 * i], prefactor_imag = pcal_corrections[2 * i + 1])
		demodulated_pcal_list.append(pipeparts.mktee(pipeline, demodulated_pcal))

	# Demodulate the pcalX channel at the lines of interest
	for i in range(len(pcalx_frequencies)):
		demodulated_pcalx = calibration_parts.demodulate(pipeline, pcalx, pcalx_frequencies[i], True, rate_out, filter_time, 0.5, prefactor_real = pcalx_corrections[2 * i], prefactor_imag = pcalx_corrections[2 * i + 1])
		demodulated_pcal_list.append(pipeparts.mktee(pipeline, demodulated_pcalx))

	cache_num = 0
	for cache, channel, in zip(gstlal_frame_cache_list, gstlal_channels):
		# Get gstlal channels from the gstlal frames
		hoft_data = pipeparts.mklalcachesrc(pipeline, location = cache, cache_dsc_regex = ifo)
		hoft_data = pipeparts.mkframecppchanneldemux(pipeline, hoft_data, do_file_checksum = False, skip_bad_files = True, channel_list = list(map("%s:%s".__mod__, channel_list)))
		hoft = calibration_parts.hook_up(pipeline, hoft_data, channel, ifo, 1.0, element_name_suffix = "_%d" % cache_num)
		hoft = calibration_parts.caps_and_progress(pipeline, hoft, "audio/x-raw,format=F64LE,channels=1,channel-mask=(bitmask)0x0", "%s_%d" % (channel, cache_num))
		deltal = pipeparts.mkaudioamplify(pipeline, hoft, arm_length)
		deltal = pipeparts.mktee(pipeline, deltal)

		for i in range(len(all_frequencies)):
			# Demodulate \DeltaL at each line
			demodulated_deltal = calibration_parts.demodulate(pipeline, deltal, all_frequencies[i], True, rate_out, filter_time, 0.5)
			# Take ratio \DeltaL(f) / pcal(f)
			deltaL_over_pcal = calibration_parts.complex_division(pipeline, demodulated_deltal, demodulated_pcal_list[i])
			# Take a running average
			deltaL_over_pcal = pipeparts.mkgeneric(pipeline, deltaL_over_pcal, "lal_smoothkappas", array_size = int(rate_out * average_time), no_default = True, filter_latency = 0.5)
			# The first samples are not averaged.  Remove only half, since sometimes early data is important.
			deltaL_over_pcal = calibration_parts.mkinsertgap(pipeline, deltaL_over_pcal, insert_gap = False, chop_length = 500000000 * average_time)
			# Find the magnitude
			deltaL_over_pcal = pipeparts.mktee(pipeline, deltaL_over_pcal)
			magnitude = pipeparts.mkgeneric(pipeline, deltaL_over_pcal, "cabs")
			# Find the phase
			phase = pipeparts.mkgeneric(pipeline, deltaL_over_pcal, "carg")
			phase = pipeparts.mkaudioamplify(pipeline, phase, 180.0 / numpy.pi)
			# Interleave
			magnitude_and_phase = calibration_parts.mkinterleave(pipeline, [magnitude, phase])
			magnitude_and_phase = pipeparts.mkprogressreport(pipeline, magnitude_and_phase, name = "progress_sink_%s_%d_%d" % (channel, cache_num, i))
			# Write to file
			pipeparts.mknxydumpsink(pipeline, magnitude_and_phase, "tests/tests_pytest/pcal_data/%s_%s_over_Pcal_%d_at_%0.1fHz.txt" % (ifo, channel.replace(' ', '_'), cache_num, all_frequencies[i]))
		cache_num = cache_num + 1

	#
	# done
	#

	return pipeline

#
# =============================================================================
#
#				     Main
#
# =============================================================================
#

#test_common.build_and_run(pcal2darm, "pcal2darm", segment = segments.segment((LIGOTimeGPS(0, 1000000000 * options.gps_start_time), LIGOTimeGPS(0, 1000000000 * options.gps_end_time))))
def Pcal2darm():
    test_common.build_and_run(pcal2darm, "pcal2darm", segment = segments.segment((LIGOTimeGPS(0, 1000000000 * options.gps_start_time), LIGOTimeGPS(0, 1000000000 * options.gps_end_time))))
    rms('P')
    rms('P', 'E')
    plot_pcal()
