#!/usr/bin/env python3
# Copyright (C) 2018  Aaron Viets, Alexander Bartoletti
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import numpy as np
import matplotlib.pyplot as plt


def plot_ASD(hoft_f='hoft_asd.txt', clean_f='clean_asd.txt'):
    hoft_f = 'tests/tests_pytest/ASD_data/' + str(hoft_f)
    clean_f = 'tests/tests_pytest/ASD_data/' + str(clean_f)
    hoft = np.loadtxt(hoft_f)
    clean = np.loadtxt(clean_f)

    hoft = np.transpose(hoft)
    clean = np.transpose(clean)

    plt.rcParams["figure.figsize"] = [7.50, 7.50]
    plt.rcParams["figure.autolayout"] = True
    hx = hoft[0]
    hy = hoft[1]
    cx = clean[0]
    cy = clean[1]

#hoft
    plt.subplot(2,1,1)
    plt.gca().set_xscale('log')
    plt.gca().set_yscale('log')
    plt.title('Hoft ASD')
    plt.plot(hx,hy, color='blue')
    plt.xlabel('Frequency')
    plt.ylabel('Strain h(f)')

#clean
    plt.subplot(2,1,2)
    plt.gca().set_xscale('log')
    plt.gca().set_yscale('log')
    plt.title('Clean ASD')
    plt.plot(cx,cy, color='red')
    plt.xlabel('Frequency')
    plt.ylabel('Strain h(f)')

    plt.savefig('tests/tests_pytest/test_ASD_plot.png')


def plot_pcal():
    #freq_list = [102.1, 1083.3, 1083.7, 17.1, 283.9, 33.4, 410.2, 410.3, 56.4, 77.7]
    freq_list = [17.1, 410.3, 1083.7]
    f_list_a = []
    f_list_e = []
    for freq in freq_list:
        f_list_a.append('tests/tests_pytest/pcal_data/H1_GDS-CALIB_STRAIN_over_Pcal_0_at_%sHz.txt' % freq)
        f_list_e.append('tests/tests_pytest/pcal_data/H1_GDS-CALIB_STRAIN_over_Pcal_1_at_%sHz.txt' % freq)
    
    plt.rcParams["figure.figsize"] = [7.50, 10]
    plt.rcParams["figure.autolayout"] = True
    
    np_lst_a = []
    cnt = 0
    for f in f_list_a:
        name = freq_list[cnt]
        cnt += 1
        arr = np.loadtxt(f)
        arr = np.transpose(arr)
        x_time = arr[0]
        y_phase = arr[2]                #magnitude and phase may be switched
        y_magnitude = arr[1]

        plt.subplot(1, 2, 1)
        plt.title('Magnitude at Frequency %s' % name)
        plt.plot(x_time, y_magnitude, color='green')
        plt.xlabel('Time')
        plt.ylabel('Magnitude')

        plt.subplot(1, 2, 2)
        plt.title('Phase at Frequency %s' % name)
        plt.plot(x_time, y_phase, color='green')
        plt.xlabel('Time')
        plt.ylabel('Phase')
        
        plt.savefig('tests/tests_pytest/test_approx_pcal_plot_%s.png' % name)
        plt.close()

    np_lst_e = []
    cnt = 0
    for f in f_list_e:
        name = freq_list[cnt]
        cnt += 1
        arr = np.loadtxt(f)
        arr = np.transpose(arr)
        x_time = arr[0]
        y_phase = arr[2]                #magnitude and phase may be switched
        y_magnitude = arr[1]

        plt.subplot(1, 2, 1)
        plt.title('Magnitude at Frequency %s' % name)
        plt.plot(x_time, y_magnitude, color='green')
        plt.xlabel('Time')
        plt.ylabel('Magnitude')

        plt.subplot(1, 2, 2)
        plt.title('Phase at Frequency %s' % name)
        plt.plot(x_time, y_phase, color='green')
        plt.xlabel('Time')
        plt.ylabel('Phase')

        plt.savefig('tests/tests_pytest/test_exact_pcal_plot_%s.png' % name)
        plt.close()


def plot_act():
    freq_list = [15.6, 16.4, 17.6]
    f_list_a = []
    f_list_e = []
    for freq in freq_list:
        f_list_a.append('tests/tests_pytest/act_data/H1_GDS-CALIB_STRAIN_over_Act_0_at_%sHz.txt' % freq)
        f_list_e.append('tests/tests_pytest/act_data/H1_GDS-CALIB_STRAIN_over_Act_1_at_%sHz.txt' % freq)

    plt.rcParams["figure.figsize"] = [7.50, 10]
    plt.rcParams["figure.autolayout"] = True

    np_lst_a = []
    acnt = 0
    for f in f_list_a:
        name = freq_list[acnt]
        acnt += 1
        arr = np.loadtxt(f)
        arr = np.transpose(arr)
        x_time = arr[0]
        y_phase = arr[2]                #magnitude and phase may be switched
        y_magnitude = arr[1]

        plt.subplot(1, 2, 1)
        plt.title('Magnitude at Frequency %s' % name)
        plt.plot(x_time, y_magnitude, color='coral')
        plt.xlabel('Time')
        plt.ylabel('Magnitude')


        plt.subplot(1, 2, 2)
        plt.title('Phase at Frequency %s' % name)
        plt.plot(x_time, y_phase, color='coral')
        plt.xlabel('Time')
        plt.ylabel('Phase')

        plt.savefig('tests/tests_pytest/test_approx_act_plot_%s.png' % name)
        plt.close()

    np_lst_e = []
    ecnt = 0
    for f in f_list_e:
        name = freq_list[ecnt]
        ecnt += 1
        arr = np.loadtxt(f)
        arr = np.transpose(arr)
        x_time = arr[0]
        y_phase = arr[2]                #magnitude and phase may be switched
        y_magnitude = arr[1]

        plt.subplot(1, 2, 1)
        plt.title('Magnitude at Frequency %s' % name)
        plt.plot(x_time, y_magnitude, color='coral')
        plt.xlabel('Time')
        plt.ylabel('Magnitude')


        plt.subplot(1, 2, 2)
        plt.title('Phase at Frequency %s' % name)
        plt.plot(x_time, y_phase, color='coral')
        plt.xlabel('Time')
        plt.ylabel('Phase')

        plt.savefig('tests/tests_pytest/test_exact_act_plot_%s.png' % name)
        plt.close()


