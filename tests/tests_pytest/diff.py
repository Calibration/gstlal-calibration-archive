#!/usr/bin/env python3
# Copyright (C) 2018  Aaron Viets, Alexander Bartoletti
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import os

def Diff():
    zeros = 0
    frames = 0

    for i in range(1370673088, 1370678400, 64):
        frames += 1            
        file1 = 'tests/tests_pytest/frames/GDS/H-H1GDS-' + str(i) + '-64.gwf'
        file2 = 'tests/tests_pytest/frames/GDS/H-H1GDS_LaterStart-' + str(i) + '-64.gwf'
        c_str = 'FrDiff -i1 {} -i2 {} -t H1:GDS-CALIB_STRAIN_CLEAN'.format(file1,file2)
        check = os.system(c_str)
        if check == 0:
            zeros += 1
    e_file = open('tests/tests_pytest/error_results.txt', 'a')
    ratio = '{}/{}'.format(zeros,frames)
    e_file.write(ratio)
    e_file.write('\n')
    ft_ratio = zeros/frames
    assert ft_ratio > 3/4
# Use unit test self.assert almost equal so it will crash if files are not the same
# assert comp, if false will crash
