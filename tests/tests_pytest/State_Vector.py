#!/usr/bin/env python3
# Copyright (C) 2018  Aaron Viets, Alexander Bartoletti
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import numpy
from math import pi
from math import erf
import resource
import datetime
import glob

import configparser

import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst
Gst.init(None)

import lal
from lal import LIGOTimeGPS

from gstlal import pipeparts
from gstlalcalibration import calibration_parts
from gstlal import simplehandler
from gstlal import datasource

from ligo import segments
from gstlalcalibration import test_common

from tests.tests_pytest.STV_error import bit_mask

from optparse import OptionParser, Option
import configparser

parser = OptionParser()

parser.add_option("--gps-start-time", metavar = "seconds", type = int, default = 1370674240, help = "GPS time at which to start processing data")
parser.add_option("--gps-end-time", metavar = "seconds", type = int, default = 1370678400, help = "GPS time at which to stop processing data")
parser.add_option("--ifo", metavar = "name", default = "H1", help = "Name of the interferometer (IFO), e.g., H1, L1")

options, filenames = parser.parse_args()

ifo = options.ifo


def state_vector(pipeline, name):
        data = pipeparts.mklalcachesrc(pipeline, location = "tests/tests_pytest/GDS_frames.cache", cache_dsc_regex = ifo)
        data = pipeparts.mkframecppchanneldemux(pipeline, data, do_file_checksum = False, skip_bad_files = True, channel_list = list(map("%s:%s".__mod__, [("H1", "GDS-CALIB_STATE_VECTOR")])))
        data = calibration_parts.hook_up(pipeline, data, "GDS-CALIB_STATE_VECTOR", ifo, 1.0)
        data = calibration_parts.caps_and_progress(pipeline, data, "audio/x-raw,format=U32LE,channels=1,channel-mask=(bitmask)0x0", "State_Vector")
        data = pipeparts.mknxydumpsink(pipeline, data, "tests/tests_pytest/%s_State_Vector.txt" % ifo)
        return pipeline


def State_Vector():
    test_common.build_and_run(state_vector, "state_vector", segment = segments.segment((LIGOTimeGPS(0, 1000000000 * options.gps_start_time), LIGOTimeGPS(0, 1000000000 * options.gps_end_time))))
    bit_mask()

